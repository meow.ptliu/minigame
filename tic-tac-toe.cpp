// Copyright 2019 P.T.Liu
#include <curses.h>

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

const int win_state[24] = {0, 1, 2,
                           3, 4, 5,
                           6, 7, 8,
                           0, 3, 6,
                           1, 4, 7,
                           2, 5, 8,
                           0, 4, 8,
                           2, 4, 6};

const char *phrase[3] = {"Lose", "Draw", "Win "};
const char *end_phrase[3] = {"You win!", "Draw!", "You lose!"};
const char mark[3] = {'X', '-', 'O'};

bool computer_first = false;
int count = 0, total = 0;

enum Token {
    X = -1,
    Freeslot,
    O
};

enum Gamestate {
    X_win = -1,
    Draw,
    O_win,
    Uncertain
};

Gamestate check_state(const Token board[9]) {
    for (int i = 0; i < 24; i += 3) {
        const int sum = board[win_state[i]] +
                        board[win_state[i + 1]] +
                        board[win_state[i + 2]];
        if (sum == -3)
            return Gamestate::X_win;
        if (sum == 3)
            return Gamestate::O_win;
    }

    for (int i = 0; i < 9; i++)
        if (board[i] == Token::Freeslot)
            return Gamestate::Uncertain;

    return Gamestate::Draw;
}

int alpha_beta(Token board[9], bool X_player, int alpha, int beta) {
    const Gamestate winner = check_state(board);
    if (winner != Gamestate::Uncertain)
        return winner;
    ++count;
    const Token player = X_player ? Token::X : Token::O;

    for (int i = 0; i < 9; i++)
        if (board[i] == Token::Freeslot) {
            board[i] = player;
            const int value = alpha_beta(board, !X_player, alpha, beta);
            board[i] = Token::Freeslot;

            if (player == Token::O) {
                alpha = max(alpha, value);
                if (alpha >= beta)
                    return beta;
            } else {
                beta = min(beta, value);
                if (beta <= alpha)
                    return alpha;
            }
        }

    if (player == Token::O)
        return alpha;
    else
        return beta;
}

inline void print_info() {
    mvprintw(3, 5, "-------------");
    mvprintw(4, 5, "| 7 | 8 | 9 |");
    mvprintw(5, 5, "-------------");
    mvprintw(6, 5, "| 4 | 5 | 6 |");
    mvprintw(7, 5, "-------------");
    mvprintw(8, 5, "| 1 | 2 | 3 |");
    mvprintw(9, 5, "-------------");
}

int move(Token board[9]) {
    clear();
    print_info();
    if (computer_first) {
        board[0] = Token::O;
        computer_first = false;
        return 0;
    }
    int best_value = -2;
    int next_move = -1;

    for (int i = 0; i < 9; i++) {
        if (board[i] == Token::Freeslot) {
            board[i] = Token::O;
            int value = alpha_beta(board, true, -2, 2);
            board[i] = Token::Freeslot;

            mvprintw((2 - i / 3) * 3 + i % 3 + 11, 40,
                     "Choose %d , result : ", (2 - i / 3) * 3 + i % 3 + 1);
            attron(COLOR_PAIR(value + 10));
            mvprintw((2 - i / 3) * 3 + i % 3 + 11, 60, phrase[value + 1]);
            attroff(COLOR_PAIR(value + 10));
            refresh();
            if (value == 1) {
                return i;
            }
            if (value > best_value) {
                best_value = value;
                next_move = i;
            }
        }
    }
    return next_move;
}

inline void print(const Token board[9]) {
    for (int i = 0; i < 9; i += 3) {
        mvprintw(2 * (i / 3) + 3, 30, "-------------");
        mvprintw(2 * (i / 3) + 4, 30,
                 "| %c | %c | %c |", mark[board[i] + 1],
                 mark[board[i + 1] + 1], mark[board[i + 2] + 1]);
    }
    mvprintw(9, 30, "-------------");
    refresh();
}

int main() {
    initscr();
    noecho();
    curs_set(0);
    start_color();

    init_pair(8, COLOR_YELLOW, COLOR_BLACK);
    init_pair(9, COLOR_RED, COLOR_BLACK);
    init_pair(10, COLOR_CYAN, COLOR_BLACK);
    init_pair(11, COLOR_GREEN, COLOR_BLACK);

    Token board[9];
    for (int i = 0; i < 9; i++)
        board[i] = Token::Freeslot;

    int sign;
    bool human = true;

    clear();
    attron(COLOR_PAIR(8));
    mvprintw(1, 15, "Play first? [Y/N]");
    attroff(COLOR_PAIR(8));
    print_info();

    for (int i = 0; i < 9; i += 3) {
        mvprintw(2 * (i / 3) + 3, 30, "-------------");
        mvprintw(2 * (i / 3) + 4, 30, "|   |   |   |");
    }

    mvprintw(9, 30, "-------------");
    refresh();

    while ((sign = getch())) {
        if (sign == 'Y' || sign == 'y') {
            human = true;
            break;
        }
        if (sign == 'N' || sign == 'n') {
            human = false;
            break;
        }
    }

    computer_first = !human;

    mvprintw(1, 15, "                 ");

    while (check_state(board) == Gamestate::Uncertain) {
        if (human) {
            print(board);

            int pos;
            do {
                do {
                    pos = getch();
                } while (pos > '9' || pos < '1');
                pos -= '0';
                pos = pos - 1 + ((pos < 4) - (pos > 6)) * 6;
            } while (board[pos] != Token::Freeslot);

            board[pos] = Token::X;
        } else {
            count = 0;
            int pos = move(board);
            total += count;

            mvprintw(11, 5, "Computer searched %d states.", count);
            mvprintw(13, 10, "Computer chooses %d.",
                     (2 - pos / 3) * 3 + pos % 3 + 1);

            board[pos] = Token::O;
        }
        human = !human;
    }

    clear();
    print_info();
    print(board);

    attron(COLOR_PAIR(11));
    mvprintw(11, 5, "A total of %d states had been searched!", total);
    attroff(COLOR_PAIR(11));

    int final = check_state(board);

    attron(COLOR_PAIR(10 - final));
    mvprintw(13, 15, "Game result : %s", end_phrase[final + 1]);
    attroff(COLOR_PAIR(10 - final));

    attron(COLOR_PAIR(8));
    mvprintw(15, 14, "Press any key to exit!", total);
    attroff(COLOR_PAIR(8));

    getch();
    endwin();

    return 0;
}
